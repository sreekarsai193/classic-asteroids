using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// ship class
/// </summary>
public class Ship : MonoBehaviour
{
    [SerializeField] GameObject prefabBullet;
    [SerializeField] GameObject HUD;

    Rigidbody2D rb2D;

    Vector2 thrustDirection;

    const float thrustForce = 5f;

   

    const float rotateDegreesPerSecond = 75;
    // Start is called before the first frame update
    void Start()
    {
        rb2D =GetComponent<Rigidbody2D>();

        thrustDirection = new Vector2(1, 0);

        
    }

    // Update is called once per frame
    void Update()
    {
        float rotationAmount = rotateDegreesPerSecond*Time.deltaTime;
        float inputRotation = Input.GetAxis("Rotate");
        if (inputRotation != 0)
        {
            if (inputRotation < 0)
            {

                rotationAmount *= -1;
            }
            transform.Rotate(Vector3.forward, rotationAmount);

            float direction = transform.eulerAngles.z;
            float angleInRadians = Mathf.Deg2Rad*direction;
            thrustDirection = new Vector2(Mathf.Cos(angleInRadians),Mathf.Sin(angleInRadians));
        }
        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            GameObject bullet=Instantiate<GameObject>(prefabBullet,transform.position,Quaternion.identity);
            bullet.GetComponent<Bullet>().ApplyForce(thrustDirection);
            AudioManager.Play(AudioClipName.PlayerShot);
        }
    }

    /// <summary>
    /// method for Physics calculations
    /// </summary>
    void FixedUpdate()
    {
        if(Input.GetAxis("Thrust")>0)
        {
            rb2D.AddForce(thrustDirection*thrustForce,ForceMode2D.Force);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Asteroid")
        {
            AudioManager.Play(AudioClipName.PlayerDeath);
            Destroy(gameObject);
            HUD.GetComponent<HUD>().StopTimer();
            
        }
    }


}
