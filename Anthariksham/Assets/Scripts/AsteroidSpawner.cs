using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]GameObject asteroidPrefab;
    void Start()
    {
        GameObject asteroid=Instantiate(asteroidPrefab,Vector3.zero,Quaternion.identity);
        float radius =asteroid.GetComponent<CircleCollider2D>().radius;
        Destroy(asteroid);

        GameObject asteroid1 = Instantiate(asteroidPrefab);
        Asteroid script1=asteroid1.GetComponent<Asteroid>();
        script1.Initialized(Direction.Up, new Vector3(0,ScreenUtils.ScreenBottom + radius,0));

        GameObject asteroid2 = Instantiate(asteroidPrefab);
        Asteroid script2 = asteroid2.GetComponent<Asteroid>();
        script2.Initialized(Direction.Down, new Vector3(0, ScreenUtils.ScreenTop - radius, 0));

        GameObject asteroid3 = Instantiate(asteroidPrefab);
        Asteroid script3 = asteroid3.GetComponent<Asteroid>();
        script3.Initialized(Direction.Left, new Vector3(ScreenUtils.ScreenRight-radius, 0, 0));

        GameObject asteroid4 = Instantiate(asteroidPrefab);
        Asteroid script4 = asteroid4.GetComponent<Asteroid>();
        script4.Initialized(Direction.Right, new Vector3(ScreenUtils.ScreenLeft + radius, 0, 0));


    }

}
