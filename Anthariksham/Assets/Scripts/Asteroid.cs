using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// script for Asteroid Behavior
/// </summary>
public class Asteroid : MonoBehaviour
{
    [SerializeField] Sprite greenAsteroid;
    [SerializeField] Sprite whiteAsteroid;
    [SerializeField] Sprite magnetaAsteroid;

    float impulseForce;
    Vector2 moveDirection;
    // Start is called before the first frame update
    void Start()
    {
       

        int asteroidColor = Random.Range(0, 3);
        if(asteroidColor==0)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = greenAsteroid;
        }
        else if(asteroidColor==1)
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = whiteAsteroid;
        }
        else
        {
            gameObject.GetComponent<SpriteRenderer>().sprite = magnetaAsteroid;
        }


    }
    public void Initialized(Direction direction,Vector3 position)
    {
        transform.position = position;
        float baseAngle = 345;
        if(direction==Direction.Up)
        {
            baseAngle = 75;
        }
        else if(direction==Direction.Left)
        {
            baseAngle = 165;
        }
        else if(direction == Direction.Down)
        {
            baseAngle = 255;
        }
        float angle = Random.Range(0, Mathf.PI/6)+baseAngle*Mathf.Deg2Rad;
        impulseForce = Random.Range(1, 4);
        moveDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

        GetComponent<Rigidbody2D>().AddForce(impulseForce * moveDirection, ForceMode2D.Impulse);
    }
    public void StartMoving(float angle)
    {
        impulseForce = Random.Range(1, 4);
        moveDirection = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));

        GetComponent<Rigidbody2D>().AddForce(impulseForce * moveDirection, ForceMode2D.Impulse);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag=="Bullet")
        {
            if (transform.localScale.x <= 0.5)
            {
                Destroy(gameObject);
                Destroy(collision.gameObject);
                
            }
            else
            {

                Vector3 asteroidScale = transform.localScale;
                asteroidScale = asteroidScale / 2;
                transform.localScale = asteroidScale;

                float astradius = GetComponent<CircleCollider2D>().radius;
                astradius = astradius / 2;
                GetComponent<CircleCollider2D>().radius = astradius;

                GameObject as1 = Instantiate<GameObject>(gameObject, transform.position, Quaternion.identity);
                GameObject as2 = Instantiate<GameObject>(gameObject, transform.position, Quaternion.identity);

                as1.GetComponent<Asteroid>().StartMoving(Random.Range(0, 2 * Mathf.PI));
                as2.GetComponent<Asteroid>().StartMoving(Random.Range(0, 2 * Mathf.PI));

                Destroy(gameObject);
                Destroy(collision.gameObject);
            }
            AudioManager.Play(AudioClipName.AsteroidHit);
        }
       
    }


}
