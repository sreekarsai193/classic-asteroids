using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is bullet class
/// </summary>
public class Bullet : MonoBehaviour
{
    const float bulletTimer=2f;

    Timer timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = gameObject.AddComponent<Timer>();
        timer.Duration = bulletTimer;
        timer.Run();
    }

    // Update is called once per frame
    void Update()
    {
        if(timer.Finished)
        {
            Destroy(gameObject);
        }
    }
    /// <summary>
    /// method to apply force to bullet
    /// </summary>
    /// <param name="diection"></param>
    public void ApplyForce(Vector2 diection)
    {
        const float impulseMagnitude = 3f;
        GetComponent<Rigidbody2D>().AddForce(impulseMagnitude*diection,ForceMode2D.Impulse);
    }

}
