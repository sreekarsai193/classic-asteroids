using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenWrapper : MonoBehaviour
{
    float colliderRadius;
    // Start is called before the first frame update
    void Start()
    {
        colliderRadius = GetComponent<CircleCollider2D>().radius;
    }
    void OnBecameInvisible()
    {
        Vector2 position = transform.position;
        if (colliderRadius + transform.position.x >= ScreenUtils.ScreenRight)
        {
            position.x = ScreenUtils.ScreenLeft + colliderRadius;
        }
        else if (transform.position.x - colliderRadius <= ScreenUtils.ScreenLeft)
        {
            position.x = ScreenUtils.ScreenRight - colliderRadius;
        }
        if (colliderRadius + transform.position.y >= ScreenUtils.ScreenTop)
        {
            position.y = ScreenUtils.ScreenBottom + colliderRadius;
        }
        else if (transform.position.y - colliderRadius <= ScreenUtils.ScreenBottom)
        {
            position.y = ScreenUtils.ScreenTop - colliderRadius;
        }
        transform.position = position;
    }

}
