using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;

    float elaspedSeconds = 0;
    bool timerRunning = true;

    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        if (timerRunning)
        {
            elaspedSeconds += Time.deltaTime;
            scoreText.text = ((int)elaspedSeconds).ToString();
        }
    }
    public void StopTimer()
    {
        timerRunning = false;
    }
}
